*** Settings ***
Documentation                                       N11 Case Study
Library                                             Collections
Library                                             Selenium2Library
Library                                             String
Variables                                           ../Recources/Variables.yaml

*** Variables ***
${browser}          chrome
${url}              https://www.n11.com/

*** Test Cases ***
Test Case
    [Documentation]            Kullanıcı Login olur, bir kelime arar, 1. ürünü seçer geri döner, Kategori seçer buradan ilk ürünü seçer, hemen al butonuna tıklar
    open browser               ${url}    ${browser}
    maximize browser window
    Close KVKK Popup
    user goes to login page
    Login                      seleniumDemo1111@gmail.com  denemeGg123  Selen Yum
    Search                     Playstation
    Click first product from search page
    Go Back
    Click Category
    Click first product from search page
    Close segmentation popup
    Click Instant Pay
    Quit Browser

Second Test Case
    [Documentation]            Kullanıcı bir kelime arar, 1. ürünü seçer geri döner, Kategori seçer buradan ilk ürünü seçer, hemen al butonuna tıklar
    open browser               ${url}    ${browser}
    maximize browser window
    Close KVKK Popup
    Search                     Playstation
    Click first product from search page
    Go Back
    Click Category
    Click first product from search page
    Close segmentation popup
    Click Instant Pay
    Quit Browser

*** Keywords ***
Close KVKK Popup
    click element  css=.btnBlack
    sleep  2

User goes to login page
    click element  css=.btnSignIn

Login
    [Arguments]  ${email}  ${password}  ${user_name}
    input text  ${Login.email}  ${email}
    input text  ${Login.password}  ${password}
    click element  ${Login.login_button}
    wait until page contains  ${user_name}

Search
    [Arguments]  ${keyword}
    input text  ${Search.productSearch}  ${keyword}
    sleep  1
    click element  ${Search.buttonProductSearch}
    ${search_result}    get text    css=h1
    log to console  ${search_result}
    wait until page contains element  css=h3.productName.bold

Click first product from search page
    ${products}    get webelements   css=h3.productName.bold
    click element  ${products[0]}

Go Back
    Execute Javascript  history.back()

Click Category
    click element  xpath=(//a[contains(.,'Video Oyun & Konsol')])[2]

Close segmentation popup
    wait until page contaions  id=sgm_img
    click element  css=.seg-popup-close

Click Instant Pay
    click element  id=instantPay

Scroll to "${Element}" element
    wait until element is visible  ${Element}
    ${Height}    Get Vertical Position    ${Element}
    ${Height}    convert to integer    ${Height}
    ${Height}    evaluate    ${Height}+150
    Execute Javascript  window.scrollTo(0, ${Height})
    sleep    1

Quit Browser
    close all browsers
